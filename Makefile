.PHONY: build run test

GO = go

build:
	$(GO) build -o app

run: build
	./app

test: build
	$(GO) test gitlab.com/the-xeptore/snapp-food-coding-challenge/b/lib
