package lib

func FindUniqueNumber(numbers []int) int {
	output := 0

	for _, number := range numbers {
		output ^= number
	}

	return output
}
