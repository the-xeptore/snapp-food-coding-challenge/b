package lib

import (
	"testing"
)

func TestFindUniqueNumber(t *testing.T) {
	t.Run("when unique number is first", func(t *testing.T) {
		numbers := []int{2, 3, 5, 4, 1, 1, 4, 5, 3}

		expected := 2

		actual := FindUniqueNumber(numbers)

		if expected != actual {
			t.Errorf("Expected: %d, got: %d", expected, actual)
		}
	})

	t.Run("when unique number is in first half", func(t *testing.T) {
		numbers := []int{1, 3, 2, 4, 5, 1, 4, 5, 3}

		expected := 2

		actual := FindUniqueNumber(numbers)

		if expected != actual {
			t.Errorf("Expected: %d, got: %d", expected, actual)
		}
	})

	t.Run("when unique number is center", func(t *testing.T) {
		numbers := []int{1, 3, 5, 4, 2, 1, 4, 5, 3}

		expected := 2

		actual := FindUniqueNumber(numbers)

		if expected != actual {
			t.Errorf("Expected: %d, got: %d", expected, actual)
		}
	})

	t.Run("when unique number is in last half", func(t *testing.T) {
		numbers := []int{1, 3, 5, 4, 5, 1, 4, 2, 3}

		expected := 2

		actual := FindUniqueNumber(numbers)

		if expected != actual {
			t.Errorf("Expected: %d, got: %d", expected, actual)
		}
	})

	t.Run("when unique number is last", func(t *testing.T) {
		numbers := []int{1, 3, 5, 4, 3, 1, 4, 5, 2}

		expected := 2

		actual := FindUniqueNumber(numbers)

		if expected != actual {
			t.Errorf("Expected: %d, got: %d", expected, actual)
		}
	})
}
