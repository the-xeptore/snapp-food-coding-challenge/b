package main

import (
	"fmt"

	"gitlab.com/the-xeptore/snapp-food-coding-challenge/b/lib"
)

func main() {
	numbers := []int{2, 2, 5, 6, 5}
	unique_number := lib.FindUniqueNumber(numbers)

	fmt.Println("Unique number is:", unique_number)
}
